// *****************************************************************************
// * Author: Lorenzo D. Moon
// * Description: Quickly create a data set for Program 3 for CS163
// *****************************************************************************

#include"header.h"

int main()
{
     // Change this if you want a different file name
    char filename[9] = "data.txt";
    cout << "Program 3 Data Maker" << endl;

    do
    {
        visit a_visit;
        create_struct(a_visit);
        save(a_visit, filename);
    }
    while(again("Add another?"));

    cout << "K THX BYE" << endl;

    return 0;
}



// Append the struct to the end of the data file
void save(visit a_visit, char * filename)
{
    ofstream file_out; // File variable

    // Open file in append mode
    file_out.open(filename, ios::app);

    // Error Checking
    if (!file_out)
        cerr << "ERROR: FILE CANNOT BE OPENED / CREATED" << endl;
    else
    {
            file_out << a_visit.city << '|'
                     << a_visit.region << '|'
                     << a_visit.todo << '|'
                     << a_visit.season << '|'
                     << a_visit.travel << '|'
                     << a_visit.other << '|' << '\n';

            cout << "Data appended to: " << filename << endl;
    }

        file_out.close();
}



// Get all the data and fill it out
void create_struct(visit & a_visit)
{
    get_line(a_visit.city, "City");
    get_line(a_visit.region, "Region");
    get_line(a_visit.todo, "Todo");
    get_line(a_visit.season, "Season");
    get_line(a_visit.travel, "Travel");
    get_line(a_visit.other, "Other");
}



// Get line from user, if you accidently hit a blank enter, it should reprompt
void get_line(char output[], const char * prompt)
{
    bool pass{true}; // Checks if the input is OK

    do
    {
        pass = true; // Reset if input was wrong once

        // Get input
        cout << prompt << ": ";
        cin.get(output, INPUT, '\n');

        // If the user just hits enter...
        if (cin.fail())
        {
            pass = false;
            cin.clear();
        }

        // If there is too much input
        if (cin.peek() != 10)
        {
            pass = false;
            cerr << "ERROR: Input too long. Max Input is " << INPUT 
                 << " chars" << endl;
        }

        cin.ignore(100, '\n'); // Clear stream
    }
    while (!pass);

    convert_to_lower(output);

    return;

}

// Prompts the user to do the action again
int again(const char * prompt)
{
    char choice{' '};    

    std::cout << "\n" << prompt << " [Y/N]: ";
    std::cin >> choice;
    std::cin.ignore(100,'\n');

    choice = toupper(choice); // Ensure YyNn are accepted

    if (choice == 'Y')
        return 1;

    if (choice == 'N')
        return 0;

    std::cout << "Invalid Response. Try Again." << std::endl;
    return again(prompt);
}



// Convert a string to lower case
int convert_to_lower(char input[])
{
    int length = strlen(input);

    for (int i = 0; i < length; ++i)
    {
        if (isalpha(input[i]))
            input[i] = tolower(input[i]);
    }

    return 0;
}
