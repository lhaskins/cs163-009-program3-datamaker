// *****************************************************************************
// * Author: Lorenzo D. Moon
// * Description: Quickly create a data set for Program 3 for CS163
// *****************************************************************************

#include <iostream>
#include <fstream>
#include <cctype>
#include <cstring>
using namespace std;

const int INPUT{50};

struct visit 
{
    char city[INPUT];
    char region[INPUT];
    char todo[INPUT];
    char season[INPUT];
    char travel[INPUT];
    char other[INPUT];
};

void create_struct(visit&);
void get_line(char output[], const char * prompt);
void save(visit a_visit, char * filename);
int convert_to_lower(char input[]);
int again(const char * prompt);
